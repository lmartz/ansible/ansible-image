FROM ubuntu:22.04

ARG KUBE_VERSION="v1.25.0"

WORKDIR /ansible

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
        apt-transport-https="$(apt-cache madison apt-transport-https | cut -d'|' -f 2 | head -n1 | xargs)" \
        # software-properties-common="$(apt-cache madison software-properties-common | cut -d'|' -f 2 | head -n1 | xargs)" \
        curl="$(apt-cache madison curl | cut -d'|' -f 2 | head -n1 | xargs)" \
        gnupg="$(apt-cache madison gnupg | cut -d'|' -f 2 | head -n1 | xargs)" \
        python3="$(apt-cache madison python3 | cut -d'|' -f 2 | head -n1 | xargs)" \
        python3-pip="$(apt-cache madison python3-pip | cut -d'|' -f 2 | head -n1 | xargs)" \
        git="$(apt-cache madison git | cut -d'|' -f 2 | head -n1 | xargs)" \
        ca-certificates="$(apt-cache madison ca-certificates | cut -d'|' -f 2 | head -n1 | xargs)" \
        # lsb-release="$(apt-cache madison lsb-release | cut -d'|' -f 2 | head -n1 | xargs)" \
        ssh="$(apt-cache madison ssh | cut -d'|' -f 2 | head -n1 | xargs)" \
        jq="$(apt-cache madison jq | cut -d'|' -f 2 | head -n1 | xargs)" \
        unzip="$(apt-cache madison unzip | cut -d'|' -f 2 | head -n1 | xargs)" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# AWS-Cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    &&  ./aws/install \
    && rm -rf awscliv2.zip

# Helm
RUN curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | tee /usr/share/keyrings/helm.gpg > /dev/null \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list \
    && apt-get update -y \
    && apt-get install --no-install-recommends helm="$(apt-cache madison helm | cut -d'|' -f 2 | head -n1 | xargs)" -y \
    && helm plugin install https://github.com/databus23/helm-diff \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Kubectl
RUN curl -LO https://dl.k8s.io/release/${KUBE_VERSION}/bin/linux/amd64/kubectl \
    && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

COPY requirements.txt .
COPY ansible-aws-requirements.txt .

# Ansible
RUN python3 -m pip install --no-cache-dir -r requirements.txt \
    && python3 -m pip install --no-cache-dir -r ansible-aws-requirements.txt \
    && ansible-galaxy collection install amazon.aws \
    && ansible-galaxy collection install kubernetes.core \
    && rm -rf ./*requirements.txt

# Permissions
RUN useradd -ms /bin/bash ansible \
    && usermod -aG ansible ansible \
    && chown ansible:ansible -R /ansible \
    && chmod 0700 -R /ansible

USER ansible

ENTRYPOINT [ "ansible-playbook" ]